package com.enterprayz.app_template;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.enterprayz.template_sdk.views.VkAuthorizeWebView;

public class MainActivity extends AppCompatActivity {
    private VkAuthorizeWebView authorizeWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniUi();
        startAuthorize();
    }

    private void iniUi() {
        authorizeWebView = (VkAuthorizeWebView) findViewById(R.id.auth_view);
    }

    private void startAuthorize() {
        authorizeWebView.makeAuthorization(new VkAuthorizeWebView.IAuthorizeHandler() {
            @Override
            public void onAuthorize(String code) {
                Log.d("CODE", code);
            }

            @Override
            public void onFail() {

            }
        });
    }
}
