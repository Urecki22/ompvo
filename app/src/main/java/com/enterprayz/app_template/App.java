package com.enterprayz.app_template;

import com.enterprayz.datacontroller_app.Interactor;
import com.enterprayz.template_sdk.RestClient;
import com.fifed.architecture_app.application.core.BaseApp;
import com.fifed.architecture_datacontroller.interactor.observer.interfaces.ObservableInteractor;

/**
 * Created by hacker on 03.11.16.
 */

public class App extends BaseApp {
    private static ObservableInteractor interactor;

    @Override
    public void onCreate() {
        super.onCreate();
        RestClient.init(BuildConfig.VERSION_NAME, context);
        interactor = Interactor.getInteractor(context, RestClient.getInstance().getApiProvider(false));
    }
}
