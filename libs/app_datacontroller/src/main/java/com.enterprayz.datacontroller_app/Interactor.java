package com.enterprayz.datacontroller_app;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.util.Log;

import com.enterprayz.datacontroller_app.account_manager.AppAccount;
import com.enterprayz.datacontroller_app.account_manager.AppAccountManager;
import com.enterprayz.datacontroller_app.actions.AutoLoginAction;
import com.enterprayz.datacontroller_app.actions.LoginWithAuthCode;
import com.enterprayz.datacontroller_app.errors.ResponseErrorHandler;
import com.enterprayz.datacontroller_app.errors.authorize.AutoAuthorizationFailed;
import com.enterprayz.datacontroller_app.models.authorize.AutoLoginModel;
import com.enterprayz.template_sdk.api.Api;
import com.enterprayz.template_sdk.beans.authorize.interfaces.IToken;
import com.fifed.architecture_datacontroller.interaction._core.Action;
import com.fifed.architecture_datacontroller.interaction._core.Error;
import com.fifed.architecture_datacontroller.interaction._core.fcm_pushes.FcmPushData;
import com.fifed.architecture_datacontroller.interaction._core.fcm_pushes.FcmTokenID;
import com.fifed.architecture_datacontroller.interaction._core.fcm_pushes.core.FcmPush;
import com.fifed.architecture_datacontroller.interactor.core.BaseInteractor;
import com.fifed.architecture_datacontroller.interactor.observer.interfaces.ObservableInteractor;


/**
 * Created by Fedir on 05.07.2016.
 */
public class Interactor extends BaseInteractor {
    private static Interactor interactor;

    protected String fcmToken;
    protected Api api;
    protected AppAccountManager accountManager;

    protected Interactor(Context context, Api api, AppAccountManager accountManager) {
        super(context);
        this.api = api;
        this.accountManager = accountManager;
    }

    public static ObservableInteractor getInteractor(Context context, Api api) {
        return (interactor == null) ? interactor = new Interactor(
                context, api, new AppAccountManager(context, context.getPackageName())) : interactor;
    }


    @CallSuper
    public void onPushEvent(FcmPush push) {
        if (push instanceof FcmTokenID) {
            fcmToken = ((FcmTokenID) push).getTokenID();
        } else if (push instanceof FcmPushData) {
            Log.v("MYLOG", ((FcmPushData) (push)).getData().toString());
        }
    }

    @Override
    @CallSuper
    public void onUserAction(Action action) {
        if (action instanceof AutoLoginAction) {
            AppAccount account = accountManager.getAppActiveAccount();
            checkAutoEntry((AutoLoginAction) action, account);
        } else {
            AppAccount acc = accountManager.getAppAccount(action.getUserID());
            if (action instanceof LoginWithAuthCode) {
                makeLoginWithAuthCode((LoginWithAuthCode) action);
            }
            //Actions with tokens
            else {
                actionWithTokens(action, acc);
            }
        }
    }

    private void actionWithTokens(Action action, AppAccount token) {

    }

    private void checkAutoEntry(AutoLoginAction autoLoginAction, AppAccount token) {
        if (token != null) {
            notifyObserversOnUpdateData(new AutoLoginModel(autoLoginAction, token.getUserId()));
        } else {
            notifyObserversOnError(new Error(autoLoginAction.getTAG(), null, new AutoAuthorizationFailed()));
        }
    }

    protected void makeLoginWithAuthCode(final LoginWithAuthCode action) {
        api.makeAuthorize(new ResponseErrorHandler<IToken>(this, action) {
            @Override
            public void onCompleted(IToken data) {
                accountManager.addAppAccount(data.getUserId(), data.getAccessToken());
            }
        }, action.getAuthCode());
    }
}
