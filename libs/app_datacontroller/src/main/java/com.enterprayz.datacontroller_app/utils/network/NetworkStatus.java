package com.enterprayz.datacontroller_app.utils.network;

public enum NetworkStatus {
    WIFI,
    MOBILE,
    NO_CONNECT
}
