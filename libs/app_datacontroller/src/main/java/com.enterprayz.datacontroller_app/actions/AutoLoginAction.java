package com.enterprayz.datacontroller_app.actions;

import com.enterprayz.datacontroller_app.models._id_interfaces.authorize.AutoLoginModelID;
import com.enterprayz.datacontroller_app.models.authorize.LoginModel;
import com.fifed.architecture_datacontroller.interaction._core.Action;

/**
 * View must implement {@link AutoLoginModelID}.
 * The result of action is {@link LoginModel}.
 * <p>
 * <p>
 * Перевірка і передача останього активного аккаунта
 */
public class AutoLoginAction extends Action {
    public AutoLoginAction(String TAG) {
        super(TAG);
    }
}