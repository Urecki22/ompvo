package com.enterprayz.datacontroller_app.actions;

/**
 * Created by hacker on 09.10.16.
 */

import com.enterprayz.datacontroller_app.models._id_interfaces.authorize.LoginModelID;
import com.enterprayz.datacontroller_app.models.authorize.LoginModel;
import com.fifed.architecture_datacontroller.interaction._core.Action;

/**
 * View must implement {@link LoginModelID}.
 * The result of action is {@link LoginModel}.
 * <p>
 */

public class LoginWithAuthCode extends Action {
    private String authCode;

    public LoginWithAuthCode(String TAG, String authCode) {
        super(TAG);
        this.authCode = authCode;
    }

    public String getAuthCode() {
        return authCode;
    }
}
