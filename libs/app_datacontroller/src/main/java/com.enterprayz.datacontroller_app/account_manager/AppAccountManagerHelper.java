package com.enterprayz.datacontroller_app.account_manager;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by hacker on 08.10.16.
 */

public abstract class AppAccountManagerHelper {
    protected static final String USER_ID = "id";
    protected static final String SERVER_TOKEN = "server_token";

    private Context context;
    private AccountManager accountManager;
    private String accountType;

    protected AppAccountManagerHelper(Context context, String accountType) {
        this.context = context;
        this.accountManager = AccountManager.get(context);
        this.accountType = accountType;
    }

    protected void updateAccount(String userName, String dataKey, String dataValue) {
        Map<String, String> userData = new HashMap<>();
        userData.put(dataKey, dataValue);
        accountManager.setUserData(new Account(userName, accountType), dataKey, dataValue);
    }

    protected void addAccount(String userName, @Nullable Map<String, String> userData) {
        Account account = new Account(userName, accountType);
        accountManager.addAccountExplicitly(account, "pass", null);
        if (userData != null) {
            for (String key : userData.keySet()) {
                accountManager.setUserData(account, key, userData.get(key));
            }
        }
    }


    protected void removeAccount(String userName) {
        accountManager.removeAccount(new Account(userName, accountType), null, null);
    }

    protected Observable<Account> getActiveAccount() {
        return getAccounts()
                .flatMap(new Func1<List<Account>, Observable<Account>>() {
                    @Override
                    public Observable<Account> call(List<Account> accounts) {
                        return Observable.from(accounts);
                    }
                });
    }

    protected Observable<Account> getAccount(String userName) {
        return getAccounts()
                .flatMap(new Func1<List<Account>, Observable<Account>>() {
                    @Override
                    public Observable<Account> call(List<Account> accounts) {
                        if (accounts.size() == 0) {
                            return Observable.just(null);
                        }
                        return Observable.from(accounts);
                    }
                })
                .filter(new Func1<Account, Boolean>() {
                    @Override
                    public Boolean call(Account account) {
                        return account == null || account.name.equals(userName);
                    }
                })
                .flatMap(new Func1<Account, Observable<Account>>() {
                    @Override
                    public Observable<Account> call(Account account) {
                        return Observable.just(account);
                    }
                });

    }

    protected Observable<List<Account>> getAccounts() {
        return Observable.just(new ArrayList<>(Arrays.asList(accountManager.getAccountsByType(accountType))));
    }


    protected Observable<String> getUserData(String userName, String key) {
        return getAccount(userName)
                .flatMap(new Func1<Account, Observable<String>>() {
                    @Override
                    public Observable<String> call(Account account) {
                        return Observable.just(accountManager.getUserData(account, key));
                    }
                });
    }
}
