package com.enterprayz.datacontroller_app.account_manager;

import android.text.TextUtils;


/**
 * Created by hacker on 09.10.16.
 */

public class AppAccount {
    private String userId;
    private String serverToken;

    public AppAccount(String userId, String serverToken) {
        this.userId = userId;
        this.serverToken = serverToken;
    }

    public String getUserId() {
        return userId;
    }

    public String getServerToken() {
        return serverToken;
    }
}
