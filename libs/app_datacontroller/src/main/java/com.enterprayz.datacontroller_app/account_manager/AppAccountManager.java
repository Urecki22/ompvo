package com.enterprayz.datacontroller_app.account_manager;

import android.accounts.Account;
import android.content.Context;
import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by hacker on 09.10.16.
 */

public class AppAccountManager extends AppAccountManagerHelper {
    private final String USER_NAME = "Liker";

    public AppAccountManager(Context context, String acountType) {
        super(context, acountType);
    }


    public void addAppAccount(int userId, String serverToken) {
        Map<String, String> data = new HashMap<>();
        data.put(AppAccountManagerHelper.USER_ID, userId + "");
        data.put(AppAccountManagerHelper.SERVER_TOKEN, serverToken);
        addAccount(USER_NAME, data);
    }


    public AppAccount getAppAccount(String userName) {
        return convertToAccount(getAccount(userName).toBlocking().first())
                .toBlocking().first();
    }

    public AppAccount getAppActiveAccount() {
        return getActiveAccount()
                .flatMap(new Func1<Account, Observable<AppAccount>>() {
                    @Override
                    public Observable<AppAccount> call(Account account) {
                        return convertToAccount(account);
                    }
                })
                .toBlocking().first();
    }

    public void removeAppAccount() {
        removeAccount(USER_NAME);
    }


    private Observable<AppAccount> convertToAccount(@NonNull Account account) {
        return Observable.just(account)
                .flatMap(new Func1<Account, Observable<AppAccount>>() {
                    @Override
                    public Observable<AppAccount> call(Account account) {
                        if (account == null) {
                            return Observable.just(null);
                        }
                        return Observable.zip(
                                getUserData(account.name, AppAccountManagerHelper.USER_ID),
                                getUserData(account.name, AppAccountManagerHelper.SERVER_TOKEN),
                                AppAccount::new);
                    }
                });
    }
}
