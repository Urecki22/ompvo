package com.enterprayz.datacontroller_app.errors;

import android.support.annotation.CallSuper;

import com.enterprayz.template_sdk._core.WebServerError;
import com.enterprayz.template_sdk._core.errors.NoInternetServerError;
import com.enterprayz.template_sdk.utils.handler.ResponseHandler;
import com.fifed.architecture_datacontroller.interaction._core.Action;
import com.fifed.architecture_datacontroller.interaction._core.Error;
import com.fifed.architecture_datacontroller.interactor.core.BaseInteractor;

/**
 * Created by hacker on 09.10.16.
 */

public abstract class ResponseErrorHandler<T> extends ResponseHandler<T> {
    private BaseInteractor interactor;
    private Action action;
    private ErrorHandler handler;

    public ResponseErrorHandler(BaseInteractor interactor, Action action) {
        this.interactor = interactor;
        this.action = action;
        this.handler = new ErrorHandler(interactor.getContext());
    }

    @Override
    @CallSuper
    public void onError(WebServerError e) {
        if (e instanceof NoInternetServerError) {
            interactor.getSync().addAction(action);
        } else
            interactor.notifyObserversOnError(new Error(action.getTAG(), handler.getErrorGlobalMessage(e), e));
    }
}

