package com.enterprayz.datacontroller_app.errors;

import android.content.Context;
import android.content.res.Resources;

import com.enterprayz.template_sdk._core.WebServerError;
import com.enterprayz.template_sdk._core.errors.NoInternetServerError;
import com.enterprayz.template_sdk._core.errors.ServerResponseError;
import com.enterprayz.template_sdk._core.errors.SocketOutServerError;
import com.fifed.architecture_datacontroller.R;

class ErrorHandler {
    private Resources resources;

    public ErrorHandler(Context context) {
        this.resources = context.getResources();
    }

    public String getErrorGlobalMessage(WebServerError e) {
        int resId = -1;
        if (e instanceof NoInternetServerError) {
            resId = R.string.error_no_internet;
        } else if (e instanceof ServerResponseError) {
            resId = R.string.error_server_response;
        } else if (e instanceof SocketOutServerError) {
            resId = R.string.error_socket_time_out;
        }
        return resId == -1 ? null : resources.getString(resId);
    }
}