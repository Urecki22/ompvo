package com.enterprayz.datacontroller_app.models.authorize;

import com.enterprayz.datacontroller_app.account_manager.AppAccount;
import com.enterprayz.datacontroller_app.models._id_interfaces.authorize.LoginModelID;
import com.fifed.architecture_datacontroller.interaction._core.Action;
import com.fifed.architecture_datacontroller.interaction._core.Model;

import java.util.List;

/**
 * /**
 * Created by hacker on 09.10.16.
 */

public class LoginModel extends Model implements LoginModelID {

    public LoginModel(Action action, List<AppAccount> accounts) {
        super(action);

    }
}
