package com.enterprayz.datacontroller_app.models.authorize;

import com.enterprayz.datacontroller_app.models._id_interfaces.authorize.AutoLoginModelID;
import com.fifed.architecture_datacontroller.interaction._core.Action;
import com.fifed.architecture_datacontroller.interaction._core.Model;

/**
 * Created by hacker on 09.10.16.
 */

public class AutoLoginModel extends Model implements AutoLoginModelID {
    private String userID;

    public AutoLoginModel(Action action, String userID) {
        super(action);
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }
}
