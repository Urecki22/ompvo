
package com.fifed.architecture_app.fragments.authorization.interfaces;

import android.content.Intent;

/**
 * Created by Fedir on 17.07.2016.
 */
public interface AuthorizationResultReceiver {
    void onAuthorizationResult(int requestCode, int resultCode, Intent data);
}
