package com.fifed.architecture_app.observers;


import com.fifed.architecture_datacontroller.interaction._core.Error;
import com.fifed.architecture_datacontroller.interaction._core.Model;

/**
 * Created by Fedir on 30.06.2016.
 */
public interface ObserverActivity {
    boolean onBackPressed();

    void onUpdateData(Model model);

    void onError(Error error);

    String getErrorTag();
}
