package com.fifed.architecture_app.fragments.authorization.core;


import com.fifed.architecture_app.activities.interfaces.feedback_interfaces.AuthActivityFragmentsFeedbacks;
import com.fifed.architecture_app.fragments.core.BaseFragment;

/**
 * Created by Fedir on 06.07.2016.
 */
public abstract class AuthorizationActivityBaseFragment extends BaseFragment {

    @Override
    public AuthActivityFragmentsFeedbacks getFragmentFeedBackInterface() {
        return (AuthActivityFragmentsFeedbacks) getActivity();
    }
}
