package com.fifed.architecture_app.fragments.core;

import android.app.Service;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.fifed.architecture_app.activities.interfaces.ActivityActionInterface;
import com.fifed.architecture_app.activities.interfaces.ActivityContentInterface;
import com.fifed.architecture_app.activities.interfaces.feedback_interfaces.core.FragmentFeedBackInterface;
import com.fifed.architecture_app.observers.ObservebleActivity;
import com.fifed.architecture_app.observers.ObserverActivity;
import com.fifed.inputlayoutviews.views.InputLayoutEditText;


/**
 * Created by Fedir on 30.06.2016.
 */
public abstract class BaseFragment extends Fragment implements ObserverActivity, View.OnClickListener{
    private String TAG;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View rootView = inflater.inflate(getResourceID(), null);
        initUI(rootView);
        setListeners();
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TAG = getClass().getSimpleName() + String.valueOf(hashCode());
        setRetainInstance(true);
    }

    @Override
    public String getErrorTag() {
        return getCustomTAG();
    }

    @LayoutRes
    protected abstract int getResourceID();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((ObservebleActivity) getActivity()).registerObserver(this);
        onFragmentRegisteredAsObserver();
    }

    @Override
    public void onResume() {
        super.onResume();
        hideKeyboard();
    }

    @Override
    public void onPause() {
        super.onPause();
        hideKeyboard();
    }

    @Override
    public void onDestroyView() {
        ((ObservebleActivity) getActivity()).unregisterObserver(this);
        onFragmentUnregisteredAsObserver();
        super.onDestroyView();
    }
    protected void hideKeyboard(){
        ((InputMethodManager)getContext().getSystemService(Service.INPUT_METHOD_SERVICE)).
                hideSoftInputFromWindow(getActivity().findViewById(android.R.id.content).getWindowToken(), 0);
    }


    public String getCustomTAG() {
        return TAG;
    }

    public FragmentFeedBackInterface getFragmentFeedBackInterface() {
        return (FragmentFeedBackInterface) getActivity();
    }

    public ActivityActionInterface getActionInterface() {
        return (ActivityActionInterface) getActivity();
    }
    protected abstract View initUI(View v);
    protected abstract void setListeners();
    protected  void onFragmentRegisteredAsObserver(){

    }
    protected void onFragmentUnregisteredAsObserver(){

    }
    protected InputLayoutEditText getInputLayoutEditText(EditText editText){
        return (InputLayoutEditText) editText.getParent().getParent();
    }

    protected ActivityContentInterface getContentInteface(){
        return (ActivityContentInterface)getActivity();
    }
    protected Toolbar getToolbar(){
        return getContentInteface().getToolbar();
    }
    protected void initBackPressed(){
        getFragmentFeedBackInterface().initBackPressed();
    }
    protected void showBackArrowOnToolbar(){
        getToolbar().setNavigationIcon(0);
        getToolbar().setLogo(0);
    }
    protected void hideBackArrowOnToolbar(){
        getToolbar().setNavigationIcon(null);
        getToolbar().setLogo(null);
    }
    protected DrawerLayout getDrawer(){
        return getContentInteface().getDrawer();
    }
    protected void showMenuIconOnToolbar(){
        getToolbar().setNavigationIcon(0);
    }
    protected void hideMenuIconOnToolbar(){
        getToolbar().setNavigationIcon(null);
    }
    protected void clearAllFragmentsBackStack(){
        FragmentManager fm = getActivity().getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }
    protected ViewGroup getToolbarContainer(){
        return ((ActivityContentInterface) getActivity()).getToolbarContainer();
    }
}
