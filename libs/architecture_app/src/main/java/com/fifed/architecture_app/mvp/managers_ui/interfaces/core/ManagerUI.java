package com.fifed.architecture_app.mvp.managers_ui.interfaces.core;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;

import com.fifed.architecture_app.constants.BaseFragmentIdentifier;
import com.fifed.architecture_app.mvp.view_data_pack.core.ViewDataPack;

/**
 * Created by Fedir on 24.07.2016.
 */
public interface ManagerUI {
    void changeFragmentTo(BaseFragmentIdentifier fragmentsID, ViewDataPack pack);
    Toolbar getToolbar();
    void initToolbar();
    void profileLogout();
    DrawerLayout getDrawer();
    ViewGroup getToolbarContainer();
    void onReceiveNotification(ViewDataPack pack);
}
