package com.fifed.architecture_app.mvp.presenters;

import com.fifed.architecture_app.mvp.presenters.intefaces.Presenter;
import com.fifed.architecture_app.mvp.views.ActivityView;
import com.fifed.architecture_datacontroller.interaction._core.Action;
import com.fifed.architecture_datacontroller.interaction._core.Error;
import com.fifed.architecture_datacontroller.interaction._core.Model;
import com.fifed.architecture_datacontroller.interactor.observer.interfaces.ObserverInteractor;

import java.lang.ref.WeakReference;


/**
 * Created by Fedir on 04.07.2016.
 */
public abstract class BaseViewPresenter implements Presenter, ObserverInteractor {
   private ActivityView activityView;

    public BaseViewPresenter(ActivityView activityView) {
        this.activityView = new WeakReference<>(activityView).get();
        registerAsObserver();
    }
    protected abstract void registerAsObserver();

    @Override
    public void onUpdateData(Model model) {
        activityView.onUpdateData(model);
    }

    @Override
    public void onError(Error error) {
        activityView.onError(error);
    }

    @Override
    public abstract  void onUserMadeAction(Action action);

    @Override
    public abstract void onPresenterDestroy();

}
