package com.fifed.architecture_app.activities.core;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import com.fifed.architecture_app.R;
import com.fifed.architecture_app.activities.interfaces.ActivityActionInterface;
import com.fifed.architecture_app.activities.interfaces.ActivityContentInterface;
import com.fifed.architecture_app.activities.interfaces.feedback_interfaces.core.FragmentFeedBackInterface;
import com.fifed.architecture_app.constants.BaseFragmentIdentifier;
import com.fifed.architecture_app.mvp.managers_ui.interfaces.core.ManagerUI;
import com.fifed.architecture_app.mvp.presenters.BaseViewPresenter;
import com.fifed.architecture_app.mvp.presenters.intefaces.Presenter;
import com.fifed.architecture_app.mvp.view_data_pack.core.ViewDataPack;
import com.fifed.architecture_app.mvp.views.ActivityView;
import com.fifed.architecture_app.observers.ObservebleActivity;
import com.fifed.architecture_app.observers.ObserverActivity;
import com.fifed.architecture_app.utils.ModelFilter;
import com.fifed.architecture_app.utils.UserSpecialInformer;
import com.fifed.architecture_datacontroller.interaction._core.Action;
import com.fifed.architecture_datacontroller.interaction._core.Error;
import com.fifed.architecture_datacontroller.interaction._core.Model;

import java.util.ArrayList;


/**
 * Created by Fedir on 30.06.2016.
 */
public abstract class BaseActivity extends AppCompatActivity implements ObservebleActivity, ActivityView, ActivityActionInterface, ActivityContentInterface, FragmentFeedBackInterface {
    private ArrayList<ObserverActivity> observerList = new ArrayList<>();
    protected ManagerUI managerUI;
    protected Presenter presenter;
    protected volatile boolean isClickedBackPressed;
    private View rootView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = findViewById(android.R.id.content);
        presenter = getViewPresenter(this);
        managerUI = getManagerUIToInit();
    }

    protected abstract ManagerUI getManagerUIToInit();

    public abstract BaseViewPresenter getViewPresenter(ActivityView activityView);

    @Override
    protected void onDestroy() {
        presenter.onPresenterDestroy();
        super.onDestroy();
    }

    public View getRootView() {
        return rootView;
    }


    @Override
    public void onBackPressed() {
        if (getDrawer() != null && getDrawer().isDrawerVisible(GravityCompat.START)) {
            getDrawer().closeDrawers();
        } else if (!notifyOnBackPressed()) {
            doubleBackPressed();
        }
    }

    protected void doubleBackPressed() {
        if (!isClickedBackPressed && getSupportFragmentManager().getBackStackEntryCount() == 0) {
            UserSpecialInformer.showInformationForUser(rootView, getString(R.string.for_exit_click_again), getSnakbarTextColor(), getSnakbarBackgroundColor());
            isClickedBackPressed = true;
            rootView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    isClickedBackPressed = false;
                }
            }, 2000);
        } else super.onBackPressed();
    }

    protected
    @ColorInt
    int getSnakbarBackgroundColor() {
        return UserSpecialInformer.DEFAULT_BACKGROUND_COLOR;
    }

    protected
    @ColorInt
    int getSnakbarTextColor() {
        return UserSpecialInformer.DEFAULT_TEXT_COLOR;
    }


    @Override
    public void registerObserver(ObserverActivity obsever) {
        observerList.add(obsever);
    }

    @Override
    public void unregisterObserver(ObserverActivity observer) {
        observerList.remove(observer);
    }

    @Override
    public boolean notifyOnBackPressed() {
        boolean fragmentGotInvoke = false;
        for (int i = 0; i < observerList.size(); i++) {
            ObserverActivity observer = observerList.get(i);
            if (fragmentGotInvoke = observer.onBackPressed()) break;
        }
        return fragmentGotInvoke;
    }

    @Override
    public void onUpdateData(Model model) {
        notifyObserversOnUpdateData(model);
    }

    @Override
    public void onError(Error error) {
        notifyObserversOnError(error);
    }

    @Override
    public void notifyObserversOnUpdateData(Model model) {
        for (int i = 0; i < observerList.size(); i++) {
            ObserverActivity observer = observerList.get(i);
            if (ModelFilter.isObserverWorkingWithModel(observer, model))
                observer.onUpdateData(model);
        }
    }

    @Override
    public void userMadeAction(Action action) {
        presenter.onUserMadeAction(action);
    }

    @Override
    public void notifyObserversOnError(Error error) {
        if (error.getGlobalErrorMessage() != null) {
            handleErrorInActivity(error);
        }
        for (int i = 0; i < observerList.size(); i++) {
            ObserverActivity observer = observerList.get(i);
            if (observer.getErrorTag().equals(error.getTAG())) observer.onError(error);
        }
    }

    @Override
    public void changeFragmentTo(BaseFragmentIdentifier fragmentsID, @Nullable ViewDataPack pack) {
        managerUI.changeFragmentTo(fragmentsID, pack);
    }

    protected void handleErrorInActivity(Error error) {
        UserSpecialInformer.showInformationForUser(rootView, error.getGlobalErrorMessage(),
                Color.RED, Color.BLACK);

    }

    @Override
    public Toolbar getToolbar() {
        return managerUI.getToolbar();
    }

    @Override
    public ViewGroup getToolbarContainer() {
        return managerUI.getToolbarContainer();
    }

    @Override
    public void initBackPressed() {
        onBackPressed();
    }

    @Override
    public void initProfileLogout() {
        managerUI.profileLogout();
    }

    public ManagerUI getManagerUI() {
        return managerUI;
    }

    @Override
    public DrawerLayout getDrawer() {
        return managerUI.getDrawer();
    }

    @Override
    public void sendNotificationToManager(ViewDataPack pack) {
        managerUI.onReceiveNotification(pack);
    }
}
