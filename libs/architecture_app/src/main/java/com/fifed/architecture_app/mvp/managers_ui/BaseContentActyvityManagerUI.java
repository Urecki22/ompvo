package com.fifed.architecture_app.mvp.managers_ui;

import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.fifed.architecture_app.R;
import com.fifed.architecture_app.mvp.managers_ui.interfaces.ManagerUIContentActivity;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;


/**
 * Created by Fedir on 01.07.2016.
 */
public abstract class BaseContentActyvityManagerUI implements ManagerUIContentActivity {
    private DrawerLayout drawer;
    private AppCompatActivity activity;
    private Toolbar toolbar;
    private ViewGroup toolbarContainer;

    public BaseContentActyvityManagerUI(AppCompatActivity activity) {
        this.activity = new WeakReference<>(activity).get();
        initUI();
        initToolbarContainer();
        initToolbar();

    }
    protected AppCompatActivity getActivity (){
        return activity;
    }

    protected abstract void initUI();

    protected abstract int getIdFragmentsContainer();

    protected void setDrawer(DrawerLayout drawer){
        this.drawer = drawer;
    }

    protected abstract int getToolbarContainerID();

    private void initToolbarContainer(){
            toolbarContainer = (ViewGroup) toolbar.findViewById(getToolbarContainerID());
    }

    protected void addFragmentToContainer(Fragment fragment, boolean toBackStack) {
        Fragment sameFragment = activity.getSupportFragmentManager()
                .findFragmentByTag(fragment.getClass().getSimpleName());
        if (!(sameFragment != null && sameFragment.isAdded())) {
            if (toBackStack)
                activity.getSupportFragmentManager().beginTransaction().setCustomAnimations(
                        R.anim.fragment_animation_enter, R.anim.fragment_animation_exit,
                        R.anim.fragment_animation_pop_enter, R.anim.fragment_animation_pop_exit)
                        .addToBackStack(fragment.getClass().getSimpleName())
                        .replace(getIdFragmentsContainer(), fragment, fragment.getClass().getSimpleName()).commit();
            else activity.getSupportFragmentManager().beginTransaction().setCustomAnimations(
                    R.anim.fragment_animation_enter, R.anim.fragment_animation_exit,
                    R.anim.fragment_animation_pop_enter, R.anim.fragment_animation_pop_exit)
                    .replace(getIdFragmentsContainer(), fragment, fragment.getClass().getSimpleName()).commit();
        }

    }
    protected void addWebFragment(Fragment fragment){
        activity.getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.fragment_animation_enter, R.anim.fragment_animation_exit,
                R.anim.fragment_animation_pop_enter, R.anim.fragment_animation_pop_exit)
                .addToBackStack(fragment.getClass().getSimpleName())
                .replace(getIdFragmentsContainer(), fragment, fragment.getClass().getSimpleName()).commit();
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }
    public void setToolbar(Toolbar toolbar){
        this.toolbar = toolbar;
    }
    @Override
    public ViewGroup getToolbarContainer(){
        return toolbarContainer;
    }

    @Override
    public abstract void initToolbar();

    protected void setToolbarListener() {
        if (toolbar != null)
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment fragment = activity.getSupportFragmentManager().findFragmentByTag
                            (getDashBoardFragmentClass().getSimpleName());
                    if (fragment != null && fragment.isVisible()) {
                        BaseContentActyvityManagerUI.this.getDrawer().openDrawer(GravityCompat.START);
                    } else activity.onBackPressed();
                }
            });
    }
    protected abstract Class<?> getDashBoardFragmentClass();

    @Override
    public void profileLogout() {

    }

    @Override
    public DrawerLayout getDrawer() {
        return drawer;
    }
    protected abstract int getToolbarNavigationIcon();

    protected void setNavigBtnToolbarLogoGravity(Toolbar toolbar) {
        toolbar.setNavigationIcon(getToolbarNavigationIcon());
        try {
            Field field = toolbar.getClass().getDeclaredField("mNavButtonView");
            field.setAccessible(true);
            try {
                ImageButton button = (ImageButton) field.get(toolbar);
                Toolbar.LayoutParams params = new Toolbar.LayoutParams(
                        Toolbar.LayoutParams.WRAP_CONTENT,
                        LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.CENTER_VERTICAL;
                button.setLayoutParams(params);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        toolbar.setNavigationIcon(null);
    }

}
