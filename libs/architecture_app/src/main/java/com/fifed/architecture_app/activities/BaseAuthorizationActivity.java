package com.fifed.architecture_app.activities;

import android.content.Intent;

import com.fifed.architecture_app.activities.core.BaseActivity;
import com.fifed.architecture_app.activities.interfaces.feedback_interfaces.AuthActivityFragmentsFeedbacks;
import com.fifed.architecture_app.mvp.managers_ui.interfaces.ManagerUIAuthActivity;


/**
 * Created by Fedir on 01.07.2016.
 */
public abstract class BaseAuthorizationActivity extends BaseActivity implements AuthActivityFragmentsFeedbacks {
    protected ManagerUIAuthActivity managerUI;


    @Override
    public void startContentActivity(int userID, Class<?> cls) {
        managerUI.startContentActivity(userID, cls);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        managerUI.authorizationResult(requestCode, resultCode, data);
    }

    @Override
    public ManagerUIAuthActivity getManagerUI() {
        return managerUI;
    }
}
