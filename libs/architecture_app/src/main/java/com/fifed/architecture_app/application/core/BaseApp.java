package com.fifed.architecture_app.application.core;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.fifed.architecture_datacontroller.interactor.core.interfaces.InteractorActionInterface;
import com.fifed.architecture_datacontroller.interactor.observer.interfaces.ObservableInteractor;


/**
 * Created by Fedir on 05.07.2016.
 */
public class BaseApp extends Application {
    protected static Context context;
    protected static ObservableInteractor interactor;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getContext() {
        return context;
    }

    public static ObservableInteractor getObservable() {
        return interactor;
    }

    public static InteractorActionInterface getActionInterface() {
        return (InteractorActionInterface) interactor;
    }
}
