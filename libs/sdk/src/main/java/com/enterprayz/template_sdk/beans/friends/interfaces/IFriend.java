package com.enterprayz.template_sdk.beans.friends.interfaces;

/**
 * Created by hacker on 03.11.16.
 */

public interface IFriend {
    int getId();

    String getFirstName();

    String getLastName();

    String getDomain();

    String getCityTitle();

    String getPhoto();

    boolean isOnline();
}
