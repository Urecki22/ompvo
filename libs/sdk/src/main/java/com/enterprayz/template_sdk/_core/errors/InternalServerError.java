package com.enterprayz.template_sdk._core.errors;

import com.enterprayz.template_sdk._core.WebServerError;

/**
 * Created by hacker on 09.10.16.
 */

public abstract class InternalServerError extends WebServerError {
    @Override
    public boolean isSeverError() {
        return false;
    }
}
