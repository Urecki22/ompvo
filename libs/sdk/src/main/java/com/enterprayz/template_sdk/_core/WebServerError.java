package com.enterprayz.template_sdk._core;

/**
 * Created by hacker on 02.10.16.
 */

public abstract class WebServerError extends RuntimeException {
    protected abstract boolean isSeverError();
}
