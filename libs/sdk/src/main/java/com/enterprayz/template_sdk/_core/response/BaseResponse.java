package com.enterprayz.template_sdk._core.response;

/**
 * Created by hacker on 07.10.16.
 */

public class BaseResponse {
    protected int code;
    protected String json_origin;

    public BaseResponse() {
    }

    public BaseResponse(int code, String json_origin) {
        this.code = code;
        this.json_origin = json_origin;
    }

    public String getJsonOrigin() {
        return json_origin;
    }

    public int getCode() {
        return code;
    }
}
