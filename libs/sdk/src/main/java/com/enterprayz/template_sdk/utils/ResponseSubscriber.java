package com.enterprayz.template_sdk.utils;

import android.support.annotation.CallSuper;

import com.enterprayz.template_sdk._core.WebServerError;
import com.enterprayz.template_sdk._core._handlers.common.ClientTimeWebErrorHandler;
import com.enterprayz.template_sdk._core._handlers.common.WebServerErrorHandler;
import com.enterprayz.template_sdk._core.response.BaseResponse;
import com.enterprayz.template_sdk.utils.handler.IResponseHandler;

import java.io.IOException;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;

public class ResponseSubscriber<T> extends Subscriber<T> implements IResponseHandler<T> {
    private IResponseHandler handler;

    public ResponseSubscriber(IResponseHandler handler) {
        this.handler = handler;
    }

    @Override
    public void onCompleted() {
        this.unsubscribe();
    }

    @Override
    public void onNext(T t) {
        if (t instanceof BaseResponse) {
            BaseResponse response = (BaseResponse) t;
            WebServerError error = new WebServerErrorHandler(response).getError();
            if (error != null) {
                onError(error);
                return;
            }
        }
        onCompleted(t);
    }

    @Override
    public void onError(Throwable e) {
        if (e instanceof HttpException && ((HttpException) e).response().errorBody() != null) {
            String errorBody;
            try {
                errorBody = ((HttpException) e).response().errorBody().string();
                WebServerError error = new WebServerErrorHandler(new BaseResponse(((HttpException) e).code(), errorBody)).getError();
                if (error == null) {
                    checkClientErrors(e);
                } else {
                    onError(error);
                }
            } catch (IOException e1) {
                checkClientErrors(e);
                e1.printStackTrace();
            }
        } else {
            checkClientErrors(e);
        }
        onCompleted();
    }


    private void checkClientErrors(Throwable e) {
        if (e instanceof WebServerError) {
            onError((WebServerError) e);
        } else {
            onError(new ClientTimeWebErrorHandler(e).getError());
        }
    }

    @Override
    @CallSuper
    public void onCompleted(T data) {
        handler.onCompleted(data);
        onCompleted();
    }

    @Override
    @CallSuper
    public void onError(WebServerError e) {
        handler.onError(e);
        onCompleted();
    }
}