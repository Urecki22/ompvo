package com.enterprayz.template_sdk.beans.authorize;

import com.enterprayz.template_sdk.beans.authorize.interfaces.IToken;

/**
 * Created by hacker on 03.11.16.
 */
/*{"access_token":"533bacf01e11f55b536a565b57531ac114461ae8736d6506a3", "expires_in":43200, '''user_id":66748} */
public class LoginResObj implements IToken {
    private String access_token;
    private long expires_in;
    private int user_id;

    @Override
    public String getAccessToken() {
        return access_token;
    }

    @Override
    public int getUserId() {
        return user_id;
    }
}
