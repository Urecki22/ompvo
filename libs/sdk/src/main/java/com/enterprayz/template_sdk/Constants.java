package com.enterprayz.template_sdk;

public class Constants {
    public static final String OAUTH_AUTHORIZE_URL_PATH = "https://oauth.vk.com/authorize";
    public static final String OAUTH_TOKEN_URL_PATH = "https://oauth.vk.com/access_token";

    public static final String FRIENDS_PATH = "friends";
}
