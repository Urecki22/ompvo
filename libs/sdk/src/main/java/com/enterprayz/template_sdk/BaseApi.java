package com.enterprayz.template_sdk;

import com.enterprayz.template_sdk.services.Authorize;
import com.enterprayz.template_sdk.utils.ResponseSubscriber;
import com.enterprayz.template_sdk.utils.handler.IResponseHandler;

import retrofit2.Retrofit;
import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.observers.SafeSubscriber;

/**
 * Created by hacker on 06.07.16.
 */
public class BaseApi {
    protected boolean isTest;
    protected Scheduler scheduler;
    protected Authorize authorizeService;

    public BaseApi(Retrofit retrofit) {
        authorizeService = retrofit.create(Authorize.class);
    }

    public class ObservableWrapper<T> {
        private IResponseHandler handler;

        public ObservableWrapper(IResponseHandler handler) {
            this.handler = handler;
        }

        public void wrap(Observable<T> observable) {
            if (isTest) {
                observable
                        .doOnNext(t -> new ResponseSubscriber<T>(handler).onNext(t))
                        .doOnError(throwable -> new ResponseSubscriber<T>(handler).onError(throwable))
                        .toBlocking().first();
            } else {
                observable
                        .subscribeOn(scheduler)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SafeSubscriber<>(new ResponseSubscriber<>(handler)));
            }
        }
    }
}
