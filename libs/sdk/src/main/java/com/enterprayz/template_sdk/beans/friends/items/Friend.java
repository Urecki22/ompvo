package com.enterprayz.template_sdk.beans.friends.items;

import com.enterprayz.template_sdk.beans.City;
import com.enterprayz.template_sdk.beans.friends.interfaces.IFriend;

/**
 * Created by hacker on 03.11.16.
 */

public class Friend implements IFriend {
    private int id;
    private String first_name;
    private String last_name;
    private String domain;
    private City city;
    private String photo;
    private boolean online;


    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getFirstName() {
        return first_name;
    }

    @Override
    public String getLastName() {
        return last_name;
    }

    @Override
    public String getDomain() {
        return domain;
    }

    @Override
    public String getCityTitle() {
        return city.getTitle();
    }

    @Override
    public String getPhoto() {
        return photo;
    }

    @Override
    public boolean isOnline() {
        return online;
    }
}
