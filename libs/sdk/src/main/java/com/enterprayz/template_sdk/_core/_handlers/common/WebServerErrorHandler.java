package com.enterprayz.template_sdk._core._handlers.common;

import android.support.annotation.Nullable;

import com.enterprayz.template_sdk._core.WebServerError;
import com.enterprayz.template_sdk._core._handlers.BodyTimeWebErrorHandler;
import com.enterprayz.template_sdk._core._handlers.WebServerCodeErrorHandler;
import com.enterprayz.template_sdk._core.response.BaseResponse;


/**
 * Created by hacker on 06.10.16.
 */

public class WebServerErrorHandler {
    private final BaseResponse response;

    public WebServerErrorHandler(BaseResponse response) {
        this.response = response;
    }

    @Nullable
    public WebServerError getError() {
        WebServerError error = new BodyTimeWebErrorHandler(response).getError();
        if (error == null) {
            error = new WebServerCodeErrorHandler(response).getError();
        }
        return error;
    }
}
