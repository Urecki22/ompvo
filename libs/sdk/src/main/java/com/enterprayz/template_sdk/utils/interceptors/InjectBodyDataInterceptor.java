package com.enterprayz.template_sdk.utils.interceptors;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by hacker on 07.10.16.
 */

public class InjectBodyDataInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);
        MediaType contentType = null;
        if (request.body() != null) {
            contentType = request.body().contentType();

        } else {
            contentType = MediaType.parse("text/plain");
        }
        String responseBody = response.body().string();
        String bodyString = injectSimpleResponseBodyAndCode(TextUtils.isEmpty(responseBody) ? "{}" : responseBody, response.code());

        ResponseBody body = ResponseBody.create(contentType, bodyString);
        return response.newBuilder().body(body).build();

    }

    private String injectSimpleResponseBodyAndCode(String bodyString, int code) {
        try {
            JSONArray array = new JSONArray(bodyString);
            return injectListResponseBodyAndCode(bodyString, array, code);
        } catch (JSONException e) {
            try {
                JSONObject object = new JSONObject(bodyString);
                object.put("json_origin", bodyString);
                object.put("code", code);
                return object.toString();
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
        return bodyString;
    }

    private String injectListResponseBodyAndCode(String bodyString, JSONArray array, int code) {
        try {
            JSONObject object = new JSONObject("{}");
            object.put("list", array);
            object.put("json_origin", "{}");
            object.put("code", code);
            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return bodyString;
    }
}
