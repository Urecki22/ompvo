package com.enterprayz.template_sdk.beans.friends;

import com.enterprayz.template_sdk._core.response.BaseResponse;
import com.enterprayz.template_sdk.beans.friends.items.Friend;

/**
 * Created by hacker on 03.11.16.
 */

public class FriendsListResObj extends BaseResponse {
    private int count;
    private Friend[] items;
}
