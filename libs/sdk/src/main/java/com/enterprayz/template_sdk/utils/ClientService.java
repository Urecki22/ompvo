package com.enterprayz.template_sdk.utils;

import android.content.Context;
import android.util.Log;

import com.enterprayz.template_sdk.utils.interceptors.HeaderInterceptor;
import com.enterprayz.template_sdk.utils.interceptors.InjectBodyDataInterceptor;
import com.enterprayz.template_sdk.utils.interceptors.LogsInterceptor;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;

public class ClientService {

    public static OkHttpClient getClient(Context context, String userAgent, String appVersion) {
        Cache cache = provideCache(context);

        return new OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .cache(cache)
                .addInterceptor(new HeaderInterceptor(userAgent, appVersion))
                .addInterceptor(new LogsInterceptor())
                .addInterceptor(new InjectBodyDataInterceptor())
                .build();
    }

    private static Cache provideCache(Context context) {
        Cache cache = null;
        try {
            cache = new Cache(new File(context.getCacheDir(), "http-cache"),
                    10 * 1024 * 1024); // 10 MB
        } catch (Exception e) {
            Log.d("Retrofit", "Could not create Cache!");
        }
        return cache;
    }
}
