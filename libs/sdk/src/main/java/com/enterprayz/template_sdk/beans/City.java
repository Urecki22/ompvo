package com.enterprayz.template_sdk.beans;

/**
 * Created by hacker on 03.11.16.
 */

public class City {
    private int id;
    private String title;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
