package com.enterprayz.template_sdk.beans.authorize.interfaces;

/**
 * Created by hacker on 03.11.16.
 */

public interface IToken {
    String getAccessToken();

    int getUserId();
}
