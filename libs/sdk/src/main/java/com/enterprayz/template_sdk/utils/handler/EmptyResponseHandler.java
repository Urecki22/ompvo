package com.enterprayz.template_sdk.utils.handler;

import android.support.annotation.CallSuper;

/**
 * Created by hacker on 07.10.16.
 */

public abstract class EmptyResponseHandler extends ResponseHandler {
    @Override
    @CallSuper
    public void onCompleted(Object data) {
        onCompleted();
    }

    public abstract void onCompleted();
}
