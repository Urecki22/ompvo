package com.enterprayz.template_sdk.beans.friends.interfaces;

import java.util.List;

/**
 * Created by hacker on 03.11.16.
 */

public interface IFriendsList {
    int getCount();
}
