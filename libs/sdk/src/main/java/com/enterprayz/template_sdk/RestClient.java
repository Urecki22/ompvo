package com.enterprayz.template_sdk;

import android.content.Context;

import com.enterprayz.template_sdk.api.Api;
import com.enterprayz.template_sdk.utils.ClientService;
import com.enterprayz.template_sdk.utils.Utils;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hacker on 06.07.16.
 */
public class RestClient {
    private static RestClient instance;
    private Context context;
    private String appVersion;
    private Api api;

    private RestClient(Context context, String appVersion) {
        Utils.ini(context);
        this.context = context;
        this.appVersion = appVersion;
    }

    public static void init(String appVersion, Context context) {
        instance = new RestClient(context, appVersion);
    }

    public static RestClient getInstance() {
        if (instance == null) {
            throw new RuntimeException("First call prepare() in Application.class");
        }
        return instance;
    }

    public Api getApiProvider(boolean isTest) {
        if (api == null) {
            api = new Api(getRetrofitCore(context, BuildConfig.USER_AGENT, appVersion), isTest);
        }
        return api;
    }

    private Retrofit getRetrofitCore(Context context, String userAgent, String appVersion) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(ClientService.getClient(context, userAgent, appVersion))
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }
}
