package com.enterprayz.template_sdk.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.enterprayz.template_sdk.BuildConfig;
import com.enterprayz.template_sdk.Constants;

/**
 * Created by hacker on 03.11.16.
 */

public class VkAuthorizeWebView extends WebView {
    private final String API_VERSION = BuildConfig.API_VERSION;
    private final String CLIENT_ID = BuildConfig.CLIENT_ID;
    private final String SUCESS_URL = BuildConfig.REDIRECT_URL;
    private final String SCOPES = "scope=" + BuildConfig.SCOPES;

    private IAuthorizeHandler authorizeHandler;
    private String currentURL;

    public VkAuthorizeWebView(Context context) {
        super(context);
        ini();
    }

    public VkAuthorizeWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        ini();
    }

    public VkAuthorizeWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        ini();
    }

    private void ini() {
        if (isInEditMode()) {
            return;
        }
        setBackgroundResource(android.R.color.white);
        setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d("Redirect to", url);
                if (url.startsWith(SUCESS_URL)) {
                    if (url.contains("code")) {
                        String code = url.replace(BuildConfig.REDIRECT_URL + "?code=", "");
                        authorizeHandler.onAuthorize(code.replaceAll(" ", ""));
                    } else {
                        authorizeHandler.onFail();
                    }
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                currentURL = url;
            }
        });

        getSettings().setLoadWithOverviewMode(true);
        getSettings().setUseWideViewPort(false);
        getSettings().setSupportZoom(false);
        getSettings().setBuiltInZoomControls(false);
        getSettings().setJavaScriptEnabled(true);
        getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        getSettings().setDomStorageEnabled(true);

    }


    public void makeAuthorization(IAuthorizeHandler handler) {
        this.authorizeHandler = handler;
        StringBuilder sb = new StringBuilder();
        sb
                .append("client_id=").append(CLIENT_ID).append("&")
                .append("display=mobile").append("&")
                .append("redirect_uri=" + SUCESS_URL).append("&")
                .append(SCOPES).append("&")
                .append("response_type=code").append("&")
                .append("v=").append(API_VERSION);

        String url = Constants.OAUTH_AUTHORIZE_URL_PATH + "?" + sb.toString();
        loadUrl(url);
    }

    public interface IAuthorizeHandler {
        /**
         * Return auth2 code
         */
        void onAuthorize(String code);

        void onFail();
    }
}
