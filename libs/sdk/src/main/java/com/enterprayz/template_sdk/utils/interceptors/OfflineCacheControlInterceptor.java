package com.enterprayz.template_sdk.utils.interceptors;

import android.text.TextUtils;


import com.enterprayz.template_sdk.utils.Utils;

import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


/**
 * Created by hacker on 19.08.16.
 */
public class OfflineCacheControlInterceptor implements Interceptor {
    /*
        max-age - Timeout value in seconds to hit again.
        no-cache - Do not use the cache.
        no-store - Do not store the cache.
        only-if-cached - Check the cache before trying to hit.
    */

    private Cache cache;

    public OfflineCacheControlInterceptor(Cache cache) {
        this.cache = cache;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        boolean reqHasOfflineCacheHeader = hasOfflineMode(request);
        boolean hasInternet = Utils.getInstiance().isNetworkAvailable();
        boolean isGetReq = request.method().equals("GET");

        if (isGetReq) {
            if (hasInternet && reqHasOfflineCacheHeader) {
                request = request
                        .newBuilder()
                        .header("Cache-Control", "no-cache")
                        .build();
            } else if (!hasInternet && reqHasOfflineCacheHeader) {
                request = request
                        .newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-stale=" + "")
                        .build();
            }
        }

        return chain.proceed(request);
    }

    private boolean hasOfflineMode(Request request) {
        String headerVal = request.headers().get("");
        return !TextUtils.isEmpty(headerVal);
    }
}
