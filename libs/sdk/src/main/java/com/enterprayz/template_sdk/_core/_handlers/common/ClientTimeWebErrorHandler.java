package com.enterprayz.template_sdk._core._handlers.common;

import com.enterprayz.template_sdk._core.WebServerError;
import com.enterprayz.template_sdk._core.errors.NoInternetServerError;
import com.enterprayz.template_sdk._core.errors.ServerResponseError;
import com.enterprayz.template_sdk._core.errors.SocketOutServerError;
import com.enterprayz.template_sdk._core.errors.UnregisterInternalError;
import com.google.gson.JsonSyntaxException;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * Created by hacker on 06.10.16.
 */

public class ClientTimeWebErrorHandler {
    private Throwable throwable;

    public ClientTimeWebErrorHandler(Throwable throwable) {
        this.throwable = throwable;
    }

    public WebServerError getError() {
        if (throwable != null) {
            if (throwable instanceof SocketTimeoutException) {
                return new SocketOutServerError();
            } else if (throwable instanceof JsonSyntaxException) {
                return new ServerResponseError();
            } else if (throwable instanceof UnknownHostException) {
                return new NoInternetServerError();
            }
            return new UnregisterInternalError(throwable);
        } else {
            return null;
        }
    }
}
