package com.enterprayz.template_sdk.services;

import com.enterprayz.template_sdk.beans.friends.FriendsListResObj;

import java.util.List;

import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by hacker on 03.11.16.
 */

public interface Friends {
    Observable<FriendsListResObj> getFriendsList(
            @Query("access_token") String token,
            @Query("user_id") int userId,
            @Query("order") String order,
            @Query("count") int count,
            @Query("offset") int offset,
            @Query("fields") List<String> fields
    );
}
