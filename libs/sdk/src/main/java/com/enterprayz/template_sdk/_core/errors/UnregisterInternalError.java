package com.enterprayz.template_sdk._core.errors;

import java.lang.*;

/**
 * Created by hacker on 06.10.16.
 */

public class UnregisterInternalError extends InternalServerError {
    private Throwable e;

    public UnregisterInternalError(Throwable e) {
        this.e = e;
    }

}
