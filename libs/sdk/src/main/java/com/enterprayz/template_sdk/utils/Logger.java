package com.enterprayz.template_sdk.utils;

import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by scijoker on 08.10.15.
 */
public class Logger {

    public static void d(String tag, String arg) {
        Log.d(tag, arg);
    }

    public static void dd(String tag, Object source) {
        if (isJSONValid(source)) {
            try {
                format(tag, new JSONObject(source.toString()).toString(2));
            } catch (JSONException e) {
                e.getCause();
            }
        } else if (source instanceof CharSequence && !TextUtils.isEmpty((CharSequence) source)) {
            format(tag, source);
        }
    }

    private static void format(String tag, Object source) {
        int spliterSize = 25;
        d(":", getSplitter(spliterSize) + tag + getSplitter(spliterSize));
        d(":", source.toString());
    }

    private static String getSplitter(int length) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            builder.append("-");
        }
        return builder.toString();
    }

    private static boolean isJSONValid(Object test) {
        try {
            new JSONObject(test.toString());
        } catch (JSONException ex) {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    new JSONArray(test);
                }
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
}