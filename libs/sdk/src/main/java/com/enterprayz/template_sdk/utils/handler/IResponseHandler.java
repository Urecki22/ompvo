package com.enterprayz.template_sdk.utils.handler;

import com.enterprayz.template_sdk._core.WebServerError;

/**
 * Created by hacker on 06.10.16.
 */

public interface IResponseHandler<T> {
    void onCompleted(T data);

    void onError(WebServerError e);
}
