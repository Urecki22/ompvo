package com.enterprayz.template_sdk.api;

import com.enterprayz.template_sdk.BaseApi;
import com.enterprayz.template_sdk.BuildConfig;
import com.enterprayz.template_sdk.beans.authorize.LoginResObj;
import com.enterprayz.template_sdk.beans.authorize.interfaces.IToken;
import com.enterprayz.template_sdk.utils.handler.IResponseHandler;

import retrofit2.Retrofit;
import rx.schedulers.Schedulers;

/**
 * Created by hacker on 06.07.16.
 */

public class Api extends BaseApi {

    public Api(Retrofit retrofit, boolean isTest) {
        super(retrofit);
        this.isTest = isTest;
        this.scheduler = isTest ? Schedulers.immediate() : Schedulers.newThread();
    }

    public void makeAuthorize(IResponseHandler<IToken> handler, String code) {
        new ObservableWrapper<LoginResObj>(handler)
                .wrap(
                        authorizeService.makeAuthorize(
                                BuildConfig.OAUTH_AUTHORIZE_URL_KEY,
                                BuildConfig.CLIENT_ID,
                                BuildConfig.CLIENT_SECRET,
                                BuildConfig.REDIRECT_URL,
                                code
                        ));
    }
}