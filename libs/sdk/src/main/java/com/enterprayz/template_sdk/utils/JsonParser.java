package com.enterprayz.template_sdk.utils;

import android.text.TextUtils;

import com.enterprayz.template_sdk._core.response.BaseResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class JsonParser {
    public static Map<String, String> parseJson(BaseResponse r) {
        Map<String, String> map = new HashMap<String, String>();
        String json = r.getJsonOrigin();
        map = JsonParser.parseJson(json);
        return map;
    }

    public static Map<String, String> parseJson(String s) {
        Map<String, String> map = new HashMap<String, String>();
        try {
            if (!TextUtils.isEmpty(s)) {
                JSONObject jsonObject = new JSONObject(s);
                Iterator keys = jsonObject.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();
                    map.put(key, jsonObject.getString(key));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return map;
    }
}
