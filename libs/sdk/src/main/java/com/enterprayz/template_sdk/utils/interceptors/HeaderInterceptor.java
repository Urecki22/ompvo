package com.enterprayz.template_sdk.utils.interceptors;


import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HeaderInterceptor implements Interceptor {
    private String userAgentCaption;
    private String userAgentVersion;

    public HeaderInterceptor(String userAgentCaption, String userAgentVersion) {
        this.userAgentCaption = userAgentCaption;
        this.userAgentVersion = userAgentVersion;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = addHeaders(chain.request());
        return chain.proceed(request);
    }

    private Request addHeaders(Request request) {
        return request.newBuilder()
                .addHeader("Accept", "application/json")
                .addHeader("User-Agent", userAgentCaption + " " + userAgentVersion)
                .method(request.method(), request.body())
                .build();
    }
}