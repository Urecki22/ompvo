package com.enterprayz.template_sdk.beans._emuns;

/**
 * Created by hacker on 03.11.16.
 */

public enum FriendOrderType {
    /**
     * сортировать по рейтингу, аналогично тому, как друзья сортируются в разделе Мои друзья (данный параметр доступен только для Desktop-приложений).
     */
    HINTS("hints"),
    /**
     * возвращает друзей в случайном порядке.
     */
    RANDOM("random"),
    /**
     * возвращает выше тех друзей, у которых установлены мобильные приложения.
     */
    MOBILE("mobile");

    String value;

    FriendOrderType(String type) {
        this.value = type;
    }

    public String getValue() {
        return value;
    }
}
