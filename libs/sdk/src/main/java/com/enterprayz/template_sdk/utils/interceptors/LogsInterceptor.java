package com.enterprayz.template_sdk.utils.interceptors;


import com.enterprayz.template_sdk.utils.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

/**
 * Created by urec on 08.10.15 urecki22@gmail.com.
 */
public class LogsInterceptor implements Interceptor {
    private long reqTime;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        makeReqLogs(request, chain);
        if (request.body() != null) {
            Response response = chain.proceed(request);
            MediaType contentType = request.body().contentType();

            String bodyString = response.body().string();
            makeResLogs(response, bodyString);
            bodyString = injectResponseBodyAndCode(bodyString, response.code());

            ResponseBody body = ResponseBody.create(contentType, bodyString);
            return response.newBuilder().body(body).build();
        } else {
            Response response = chain.proceed(request);
            makeResLogs(response, "no body");
            return response;
        }
    }

    private String injectResponseBodyAndCode(String bodyString, int code) {
        try {
            JSONObject object = new JSONObject(bodyString);
            object.put("json_origin", bodyString);
            object.put("code", code);
            return object.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return bodyString;
    }

    private void makeReqLogs(Request request, Chain chain) {
        reqTime = System.currentTimeMillis();
        String sendReq =
                "to " + request.url() +
                        "\n" + chain.connection() +
                        "\n" + request.headers();
        Logger.dd(request.method(), sendReq);
        String body = bodyToString(request);
        Logger.dd("BODY", body);

    }

    private static String bodyToString(final Request request) {
        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "no body";
        } catch (final NullPointerException e) {
            return "no body";
        }
    }

    private void makeResLogs(Response response, String responseBodyString) {
        long t2 = System.currentTimeMillis();

        String resRes = "RECEIVED " +
                "\nfrom " + response.request().url() +
                "\ncode  " + response.code() +
                "\nwith " + (t2 - reqTime) + "ms";

        Logger.dd("RESPONCE", resRes);
        Logger.dd("HEADERS", "/" + response.headers());
        Logger.dd("BODY", responseBodyString);
        Logger.dd("---", "`" + "\n`" + "\n`" + "\n`" + "\n`");
    }
}
