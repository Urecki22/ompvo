package com.enterprayz.template_sdk.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by hacker on 19.08.16.
 */
public class Utils {
    private Context c;
    private static Utils utils;
    private final String DOMAIN_NAME_PATTERN = "^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}$";
    private Pattern pDomainNameOnly = Pattern.compile(DOMAIN_NAME_PATTERN);

    private Utils(Context c) {
        this.c = c;
    }

    public static void ini(Context c) {
        utils = new Utils(c);
    }

    public static Utils getInstiance() {
        return utils;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public String convertToBase64(String string) {
        byte[] authEncBytes = Base64.encode(string.getBytes(), Base64.DEFAULT);
        String authStringEnc = new String(authEncBytes);
        System.out.println("Base64 encoded auth string: " + authStringEnc);
        return authStringEnc.trim();
    }

    public boolean isDomainName(String domainName) {
        return pDomainNameOnly.matcher(domainName).find();
    }

    public static Date getDateFromString(String data, String pattern) {
        DateFormat format = new SimpleDateFormat(pattern, Locale.ENGLISH);
        try {
            return format.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
