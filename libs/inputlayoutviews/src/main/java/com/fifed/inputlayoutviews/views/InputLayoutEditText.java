package com.fifed.inputlayoutviews.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Service;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fifed.inputlayoutviews.R;
import com.fifed.inputlayoutviews.utils.validators.ValidatorEmptyText;
import com.fifed.inputlayoutviews.utils.validators.core.TextValidator;
import com.fifed.inputlayoutviews.utils.validators.core.ValidatorResponse;

import java.util.ArrayList;


/**
 * Created by Fedir on 23.07.2016.
 */
public class InputLayoutEditText extends LinearLayout implements View.OnFocusChangeListener, TextWatcher {
    private EditText editText;
    private TextView tvHint, tvError;
    private ArrayList<TextValidator> finishingValidatorList = new ArrayList<>();
    private ArrayList <TextValidator> runtimeValidatorList = new ArrayList<>();
    private boolean srarted, hadFocus, isError;
    private InputMethodManager imm;
    private final float floatingDistance = - (30f * getResources().getDisplayMetrics().density + 0.5f);

    public InputLayoutEditText(Context context) {
        super(context);
        initUI(context);
        initAdditionalViews();
    }

    public InputLayoutEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI(context);
        initCustomAttributes(context, attrs);
        initAdditionalViews();
    }

    public InputLayoutEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUI(context);
        initAdditionalViews();
    }

    private void initUI(Context context){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View v = inflater.inflate(R.layout.custom_edit_text_in_input_layout, null);
        editText = (EditText) v.findViewById(R.id.base_edit_text);
        editText.setOnFocusChangeListener(this);
        editText.addTextChangedListener(this);
        tvHint = (TextView) v.findViewById(R.id.fifed_inputlayout_textview_hint);
        tvError = (TextView) v.findViewById(R.id.fifed_inputlayout_textview_error);
        addView(v, new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    public void checkEditText(){
        if(editText.getText().length() != 0){
            tvHint.animate().translationY(floatingDistance)
                    .scaleX(0.7f).scaleY(0.7f).setDuration(200);
        } else if(!editText.hasFocus()) tvHint.animate().translationY(0).scaleX(1).scaleY(1).setDuration(200);
    }

    public void setError(@Nullable String error){
        if(error == null){
            isError = false;
            tvError.animate().scaleY(0).scaleX(0).setDuration(200).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    invalidate();
                }
            }).start();
        } else {
            isError = true;
            tvError.setText(error);
            tvError.animate().scaleY(1).scaleX(1).setDuration(200).start();
        }
    }
    public boolean isErrorShowing(){
        return isError;
    }
    public void setErrorColor(@ColorInt int color){
        tvError.setTextColor(color);
    }
    public void setFloatingHintColor(@ColorInt int color){
        tvHint.setTextColor(color);
    }


    private void initCustomAttributes(Context context, AttributeSet attrs) {
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.InputLayoutEditText);
        setEmtyTextValidator(attributes);
        initInputType(attributes);
        setBackgroundSolidEditText(attributes);
        setTexColorEditText(attributes);
        initHint(attributes);
        setHintColor(attributes);
        setPaddingToEditText(attributes);
        setEditTextHeight(attributes);
        setEditTextWidth(attributes);
        attributes.recycle();
    }
    private void initInputMethodManager(){
        imm = (InputMethodManager)getContext().getSystemService(Service.INPUT_METHOD_SERVICE);
    }

    private void setEditTextHeight(TypedArray attributes){
       float height = attributes.getDimension(R.styleable.InputLayoutEditText_edit_text_height, 0);
        if(height > 0){
            ViewGroup.LayoutParams params = editText.getLayoutParams();
            params.height = (int) height;
            editText.setLayoutParams(params);
            requestLayout();
        }
    }

    private void setEditTextWidth(TypedArray attributes){
        float width = attributes.getDimension(R.styleable.InputLayoutEditText_edit_text_width, 0);
        if(width > 0){
            ViewGroup.LayoutParams params = editText.getLayoutParams();
            params.width = (int) width;
            editText.setLayoutParams(params);
            requestLayout();
        }
    }

    private void setBackgroundSolidEditText(TypedArray attributes){
        @DrawableRes
        int etBachground = attributes.getResourceId(R.styleable.InputLayoutEditText_background_edit_text, R.drawable.text_input_layout_border_transparent);
        editText.setBackgroundResource(etBachground);
    }
    private void setTexColorEditText(TypedArray attributes){
        @ColorInt
        int color = attributes.getColor(R.styleable.InputLayoutEditText_edit_text_color_text, Color.BLACK);
        editText.setTextColor(color);
    }
    private void setHintColor(TypedArray attributes){
        @ColorInt
        int color = attributes.getColor(R.styleable.InputLayoutEditText_hint_color, ResourcesCompat.getColor(getResources(),
                R.color.blue_light_order_subject, null));
        setFloatingHintColor(color);
    }

    private void setEmtyTextValidator(TypedArray attributes){
        boolean mightBeEmpty = attributes.getBoolean(R.styleable.InputLayoutEditText_might_be_empty, false);
        if(!mightBeEmpty)addRuntimeValidator(new ValidatorEmptyText());
    }

    private void initInputType(TypedArray attributes){
        int inputType = attributes.getInteger(R.styleable.InputLayoutEditText_custom_input_type, 1);
        switch (inputType){
            case 1 : editText.setInputType(InputType.TYPE_CLASS_TEXT);

                break;
            case 2:
                editText.setInputType(InputType.TYPE_CLASS_PHONE);

                break;
            case 3 :
                editText.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                editText.setTransformationMethod(PasswordTransformationMethod.getInstance());

                break;
            case 4 :
                editText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

                break;

        }
    }
    private void initAdditionalViews(){
        tvHint.setPivotX(0);
        tvError.setPivotX(0);
        tvError.animate().scaleY(0).scaleX(0).setDuration(0).start();
    }
    private void setPaddingToEditText(TypedArray attributes){

        int right = attributes.getInt(R.styleable.InputLayoutEditText_edit_text_padding_right, -1);
        int left = attributes.getInt(R.styleable.InputLayoutEditText_edit_text_padding_left, -1);
        int top = attributes.getInt(R.styleable.InputLayoutEditText_edit_text_padding_top, -1);
        int bottom = attributes.getInt(R.styleable.InputLayoutEditText_edit_text_padding_bottom, -1);
        editText.setPadding(left > - 1 ? left : editText.getPaddingLeft(), top > - 1 ? top : editText.getPaddingTop(),
                right > - 1 ? right : editText.getPaddingRight(), bottom > - 1 ? right : editText.getPaddingBottom());
    }
    private  void initHint(TypedArray attributes){
        String hint = attributes.getString(R.styleable.InputLayoutEditText_text_hint);
        if(hint != null){
            tvHint.setText(hint);
        }
    }
    public void addFinishingValidator(TextValidator validtor){
        finishingValidatorList.add(validtor);
    }
    private boolean getVisibilityKeyBoard(){
        Rect r = new Rect();
        View mRootView = getRootView();
        mRootView.getWindowVisibleDisplayFrame(r);
        int heightDiff = mRootView.getRootView().getHeight() - (r.bottom - r.top);
        float dp = heightDiff / getResources().getDisplayMetrics().density;;
        return (dp > 200);
    }

    public void addRuntimeValidator(TextValidator validtor){
        runtimeValidatorList.add(validtor);
        finishingValidatorList.add(validtor);
    }

    public boolean verifyFieldWithAllFinishingValidators(){
        for(int i = 0; i < finishingValidatorList.size(); i++){
            ValidatorResponse response = finishingValidatorList.get(i).isValidText(editText.getText().toString(), getContext());
            setError(response.getError());
            if(!response.isValid()) return false;
        }
        return  true;
    }
    private void verifyFieldWithRuntimeValidators(){
        for(int i = 0; i < runtimeValidatorList.size(); i++){
            ValidatorResponse response = runtimeValidatorList.get(i).isValidText(editText.getText().toString(), getContext());
            setError(response.getError());
            if(!response.isValid()) break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        checkEditText();
        setError(null);
        if(srarted && hadFocus){
            verifyFieldWithRuntimeValidators();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    protected void onDetachedFromWindow() {
        finishingValidatorList.clear();
        runtimeValidatorList.clear();
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        hadFocus = false;
        super.onDetachedFromWindow();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        initInputMethodManager();
        checkEditText();
        srarted = true;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        hadFocus = true;
        if(hasFocus && editText.getText().length() == 0){
            tvHint.animate().translationY(floatingDistance).
                    scaleX(0.7f).scaleY(0.7f).setDuration(200).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    imm.showSoftInput(editText, 0);
                }
            });
            if(!getVisibilityKeyBoard() && srarted) {
                postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                    }
                }, 1);
            }
        } else if (editText.getText().length() == 0 && tvError.getVisibility() != VISIBLE){
            tvHint.animate().translationY(0).scaleX(1).scaleY(1).setDuration(200);
        }
    }
}
