package com.fifed.inputlayoutviews.utils.validators;

import android.content.Context;

import com.fifed.inputlayoutviews.R;
import com.fifed.inputlayoutviews.utils.validators.core.TextValidator;
import com.fifed.inputlayoutviews.utils.validators.core.ValidatorResponse;

/**
 * Created by Fedir on 04.08.2016.
 */
public class ValidatorPassword implements TextValidator {
    @Override
    public ValidatorResponse isValidText(String text, Context context) {
        if (text.length() > 0 && text.length() < 256) {
            return new ValidatorResponse(null, true);
        } else return new ValidatorResponse(context.getString(R.string.error_valid_password), false);
    }
}
