package com.fifed.architecture_datacontroller;

import com.enterprayz.template_sdk.RestClient;
import com.enterprayz.template_sdk.api.Api;
import com.enterprayz.template_sdk._core.WebServerError;
import com.fifed.architecture_datacontroller.utils.ManifestIncludeRobolectricRunner;
import com.fifed.architecture_datacontroller.utils.TestEmptyResponseHandler;
import com.fifed.architecture_datacontroller.utils.TestResponseHandler;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

@RunWith(ManifestIncludeRobolectricRunner.class)
@Config(sdk = 21, shadows = ManifestIncludeRobolectricRunner.NetworkSecurityPolicy.class)

public class HostingRequestsTest {

    private Api api;
    private final String userName = "tw4year";
    private final String userPass = "123456789";
    private final String userPhoneRegID = "e0mkt71JdZM:APA91bFJGq6n5kp2PIzCJEqWnAqDs9APJxPUHBahMgt7-JGiPdz7fWO68HvheEY0gj2az5K_KqgkBVDapKxuvGpPim0LOmsu-H-eqycczmFpsgdCBJn2VOupBFkKD_hmjeEG7_ixT_xt";
    private String token;

    public HostingRequestsTest() {
        RestClient.init("HostingUnitTesting", RuntimeEnvironment.application.getApplicationContext());
        api = RestClient.getInstance().getApiProvider(true);
        api.signInWithLogin(new TestResponseHandler<ProfileModifyObj>() {
            @Override
            public void onCompleted(ProfileModifyObj data) {
                super.onCompleted(data);
                token = data.getServerToken();
            }
        }, userName, userPass, userPhoneRegID);
    }

    @Before
    public void ini() {
        ShadowLog.stream = System.out;
    }


    @Test
    public void s1_signInWithUnsupportedAccount() throws Exception {
        api.signInWithLogin(new TestResponseHandler<ProfileModifyObj>() {
            @Override
            public void onError(WebServerError e) {
                boolean isCondition = e instanceof UnavailableAccountTypeError;
                Assert.assertTrue(isCondition);
            }
        }, "cj50640", "8AwZNpwq", userPhoneRegID);
    }
/*
    @Test
    public void s2_signInWithCaptchaConfirmAccount() throws Exception {
        api.signInWithLogin(new TestResponseHandler<ProfileModifyObj>(), "bvblogic", "qwerty123", userPhoneRegID);
    }*/

    @Test
    public void s3_getAccountByRecoveryToken() throws Exception {
        api.singInWithToken(new TestResponseHandler<ProfileModifyObj>(), token.substring(7));
    }

    @Test
    public void s4_sendPasswordRecoveryMail() {
        api.sendEmailWithPasswordRecoveryToken(new TestEmptyResponseHandler(), userName);
    }

    @Test
    public void s5_getProfileInfo() {
        api.getProfileInfo(new TestResponseHandler<ProfileModifyObj>() {
            @Override
            public void onCompleted(ProfileModifyObj data) {
                data.getAvatar();
            }
        }, userName, token);
    }

    @Test
    public void s5_getServerNotifications() {
        api.getHostingServerNotifications(new TestResponseHandler<ServerNotificationsListResObj>() {
            @Override
            public void onCompleted(ServerNotificationsListResObj data) {
                super.onCompleted(data);
            }
        }, userName, token);
    }

    /* @Test
     public void s5_1_removeServerNotifications() {
         api.removeServerNotification(new TestEmptyResponseHandler(), "hosting-dev", 14764598715L, token);
     }
 */
    @Test
    public void s6_getDomains() {
        api.getDomains(new TestResponseHandler<DomainsListModifyObj>() {
            @Override
            public void onCompleted(DomainsListModifyObj data) {
                super.onCompleted(data);
            }
        }, userName, token);
    }

    @Test
    public void s7_getHostingServerState() {
        api.getHostingServerServiceState(new TestResponseHandler<>(), userName, token);
    }

    @Test
    public void s7_getHostingServerCapacity() {
        api.getHostingServerServiceCapacity(new TestResponseHandler<ServerServiceLoadCapacityModifyObj>() {
            @Override
            public void onCompleted(ServerServiceLoadCapacityModifyObj data) {
                Assert.assertTrue(data.getCpuLoadCapacity().size() > 0);
                Assert.assertTrue(data.getMySqlLoadCapacity().size() > 0);
            }
        }, userName, token);
    }

    @Test
    public void s8_getDomainStatistic() {
        api.getDomains(new TestResponseHandler<DomainsListModifyObj>() {
            @Override
            public void onCompleted(DomainsListModifyObj data) {
                api.getDomainStatistic(new TestResponseHandler<DomainStatisticResObj>() {
                    @Override
                    public void onCompleted(DomainStatisticResObj data) {
                        Assert.assertTrue(data.getStatistic().size() > 0);
                    }
                }, data.getDomains().get(0).getDomainTitle(), userName, token);
                api.getDomainStatistic(new TestResponseHandler<DomainStatisticResObj>() {
                    @Override
                    public void onCompleted(DomainStatisticResObj data) {
                        Assert.assertTrue(data.getStatistic().size() > 0);
                    }
                }, data.getDomains().get(1).getDomainTitle(), userName, token);
            }
        }, userName, token);
    }

    @Test
    public void s9_getAccountTariffPlan() {
        api.getHostingAccountTariffPlan(new TestResponseHandler<AccountTariffPlanModifyObj>() {
            @Override
            public void onCompleted(AccountTariffPlanModifyObj data) {
                data.getLikeTariffPlan();
            }
        }, userName, token);
    }

    @Test
    public void s10_getBalance() {
        api.getHostingBalance(new TestResponseHandler<HostingBalance>() {
            @Override
            public void onCompleted(HostingBalance data) {
                data.getBalance();
            }
        }, userName, token);
    }
}