package com.fifed.architecture_datacontroller.utils;

import com.enterprayz.template_sdk._core.WebServerError;
import com.enterprayz.template_sdk.utils.handler.EmptyResponseHandler;

/**
 * Created by hacker on 07.10.16.
 */

public class TestEmptyResponseHandler extends EmptyResponseHandler {

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(WebServerError e) {

    }
}
