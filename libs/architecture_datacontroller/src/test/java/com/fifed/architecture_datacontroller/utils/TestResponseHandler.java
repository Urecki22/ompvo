package com.fifed.architecture_datacontroller.utils;

import com.enterprayz.template_sdk._core.WebServerError;
import com.enterprayz.template_sdk.beans.errors.server.authorize.NeedCaptchaConfirmError;
import com.enterprayz.template_sdk.beans.errors.server.authorize.NeedSmsConfirmError;
import com.enterprayz.template_sdk.utils.handler.ResponseHandler;

import org.junit.Assert;

/**
 * Created by hacker on 07.10.16.
 */

public class TestResponseHandler<T> extends ResponseHandler<T> {
    @Override
    public void onCompleted(T data) {

    }

    @Override
    public void onError(WebServerError e) {
        if (e instanceof NeedSmsConfirmError || e instanceof NeedCaptchaConfirmError) {
            Assert.assertTrue(true);
        }
    }
}
