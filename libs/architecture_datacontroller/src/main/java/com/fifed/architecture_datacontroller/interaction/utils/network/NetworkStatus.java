package com.fifed.architecture_datacontroller.interaction.utils.network;

public enum NetworkStatus {
    WIFI,
    MOBILE,
    NO_CONNECT
}
