package com.fifed.architecture_datacontroller.interaction._core;

/**
 * Created by Fedir on 05.07.2016.
 */
public abstract class Action {
    protected String userID;
    private String TAG;
    protected boolean hasOfflineSync;

    public Action(String TAG) {
        this.TAG = TAG;
    }

    public Action(String TAG, String userID) {
        this.TAG = TAG;
        this.userID = userID;
    }

    public String getTAG() {
        return TAG;
    }

    public String getUserID() {
        return userID;
    }

    public boolean hasOfflineSync() {
        return hasOfflineSync;
    }
}
