package com.fifed.architecture_datacontroller.interaction._core.fcm_pushes;

import com.fifed.architecture_datacontroller.interaction._core.fcm_pushes.core.FcmPush;

import java.util.Map;


/**
 * Created by Fedir on 10.07.2016.
 */
public class FcmPushData implements FcmPush {
    Map<String, String> data;

    public Map<String, String> getData() {
        return data;
    }

    public FcmPushData setData(Map<String, String> data) {
        this.data = data;
        return this;
    }
}
