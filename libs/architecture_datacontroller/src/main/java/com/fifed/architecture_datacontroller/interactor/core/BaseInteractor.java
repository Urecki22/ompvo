package com.fifed.architecture_datacontroller.interactor.core;

import android.content.Context;

import com.fifed.architecture_datacontroller.interaction._core.Error;
import com.fifed.architecture_datacontroller.interaction._core.fcm_pushes.core.FcmPush;
import com.fifed.architecture_datacontroller.interaction._core.Model;
import com.fifed.architecture_datacontroller.interaction.utils.OfflineActionsSync;
import com.fifed.architecture_datacontroller.interaction.utils.network.NetworkStatus;
import com.fifed.architecture_datacontroller.interactor.core.interfaces.InteractorActionInterface;
import com.fifed.architecture_datacontroller.interactor.observer.interfaces.ObservableInteractor;
import com.fifed.architecture_datacontroller.interactor.observer.interfaces.ObserverInteractor;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;


/**
 * Created by Fedir on 05.07.2016.
 */
public abstract class BaseInteractor implements ObservableInteractor, InteractorActionInterface {
    private ArrayList<ObserverInteractor> observerList = new ArrayList<>();
    protected Context context;
    private OfflineActionsSync sync;

    protected BaseInteractor(Context context) {
        this.context = context;
        this.sync = OfflineActionsSync.getInstance(this);
        EventBus.getDefault().register(this);
    }

    @Override
    public void registerObserver(ObserverInteractor observer) {
        observerList.add(observer);
    }

    @Override
    public void unregisterObserver(ObserverInteractor observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyObserversOnUpdateData(Model model) {
        for (int i = 0; i < observerList.size(); i++) {
            ObserverInteractor observer = observerList.get(i);
            observer.onUpdateData(model);
        }
    }

    @Override
    public void notifyObserversOnError(Error error) {
        for (int i = 0; i < observerList.size(); i++) {
            ObserverInteractor observer = observerList.get(i);
            observer.onError(error);
        }
    }

    @Subscribe
    public void onPushEvent(FcmPush push) {

    }

    @Subscribe
    public void onChangeNetworkStatus(NetworkStatus status) {
        sync.onChangeNetworkState(status);
    }

    public Context getContext() {
        return context;
    }


    public OfflineActionsSync getSync() {
        return sync;
    }
}

