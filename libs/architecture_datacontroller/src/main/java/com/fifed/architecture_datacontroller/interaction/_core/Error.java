package com.fifed.architecture_datacontroller.interaction._core;


import com.enterprayz.template_sdk._core.WebServerError;

/**
 * Created by Fedir on 05.07.2016.
 */
public class Error {
    private String TAG;
    private String globalErrorMessage;
    private WebServerError error;

    public Error(String TAG, String globalErrorMessage, WebServerError error) {
        this.TAG = TAG;
        this.globalErrorMessage = globalErrorMessage;
        this.error = error;
    }


    public String getTAG() {
        return TAG;
    }

    public String getGlobalErrorMessage() {
        return globalErrorMessage;
    }

    public WebServerError getServerError() {
        return error;
    }

}
