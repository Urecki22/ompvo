package com.fifed.architecture_datacontroller.interaction.utils.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.greenrobot.eventbus.EventBus;

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        NetworkStatus status = NetworkUtil.getConnectivityStatus(context);
        EventBus.getDefault().post(status);
    }
}