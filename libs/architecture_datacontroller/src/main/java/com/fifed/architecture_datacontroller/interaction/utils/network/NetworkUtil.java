package com.fifed.architecture_datacontroller.interaction.utils.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {

    public static NetworkStatus getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return NetworkStatus.WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return NetworkStatus.MOBILE;
        }
        return NetworkStatus.NO_CONNECT;
    }
}