package com.fifed.architecture_datacontroller.interaction.utils;

import com.fifed.architecture_datacontroller.interaction._core.Action;
import com.fifed.architecture_datacontroller.interaction.utils.network.NetworkStatus;
import com.fifed.architecture_datacontroller.interactor.core.BaseInteractor;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by hacker on 23.08.16.
 */
public class OfflineActionsSync {
    private static OfflineActionsSync instance;
    private BaseInteractor interactor;
    private Map<String, Action> actionsToSync = new HashMap<>();

    private OfflineActionsSync(BaseInteractor interactor) {
        this.interactor = interactor;
    }


    public static OfflineActionsSync getInstance(BaseInteractor interactor) {
        if (instance == null) {
            instance = new OfflineActionsSync(interactor);
        }
        return instance;
    }

    public void addAction(Action action) {
        if (action.hasOfflineSync()) {
            actionsToSync.put(action.getTAG(), action);
        }
    }


    public void onChangeNetworkState(NetworkStatus status) {
        if (status.equals(NetworkStatus.MOBILE) || status.equals(NetworkStatus.WIFI)) {
            makeSync();
        }
    }

    private void makeSync() {
        for (Action action : actionsToSync.values()) {
            interactor.onUserAction(action);
            actionsToSync.remove(action.getTAG());
        }
    }
}
