package com.fifed.architecture_datacontroller.interactor.observer.interfaces;

import com.fifed.architecture_datacontroller.interaction._core.Error;
import com.fifed.architecture_datacontroller.interaction._core.Model;

/**
 * Created by Fedir on 05.07.2016.
 */
public interface ObserverInteractor {
    void onUpdateData(Model model);

    void onError(Error error);
}
